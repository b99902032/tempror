// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .
var t = 1800

function TimeEntry(){
    var c=setTimeout("startTime()",1000)
}


function startTime(){
    var a=Math.round((t-30)/60)
    var m=checkTime(a)
    var s=checkTime(t%60)
    document.getElementById('txt').innerHTML=m+":"+s
    t=t-1
}


function checkTime(i){
    if (i<10) {
        i="0" + i
    }
    return i
}

function timedMsg(ans, correctAns) {
    //console.log(ans);
    if(ans === correctAns) alert('Correct answer');
    else alert('Wrong answer');
}

