class BattleController < ApplicationController
  def index
    @Q_num  = Question.count
    @battle = Question.find(@Q_num)
    @answer = @battle.answer
    respond_to do |format|
      format.html
      format.json { render :nothing => true }
    end
  end
  
  def show
  
  end
end
